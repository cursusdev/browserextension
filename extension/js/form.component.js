const templateForm = document.createElement('template');
templateForm.innerHTML = `
    <style type="text/css">
        #container {
            width: 400px;
        }
        .form-control.form-control-sm.userTag {
            width: 200px;
        }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <div id="container">
        <br />
        <!-- <input class="form-control form-control-sm userTag" type="text" placeholder="Entrer votre Pseudo!"><br /> -->
        <h2>Bonjour Cursus</h2>
        <div id="content">
            <input type="text" class="form-control title" placeholder="Quel titre donnez-vous!" aria-label="TitreName" aria-describedby="addon-wrapping" >
            <br />
        </div>
        <div id="img">
            <!-- <span>Télécharger votre image!</span>
            <button id="download" onchange="_fileSelect(event);">Parcourir</button>
            <span>ou :</span> -->
            <div class="input-group flex-nowrap">
            <div class="input-group-prepend">
                <span class="input-group-text" id="addon-wrapping">Img</span>
            </div>
            <input type="text" class="form-control imgTag" placeholder="Entrer l'url de votre image!" aria-label="Username" aria-describedby="addon-wrapping" >
            </div>
            <br />
            <span id="file"></span>
        <div>
    </div>
`;

export default class FormTag extends HTMLElement {
    constructor() {
        super();
        this._shadowRoot = this.attachShadow({mode: 'open'});
        this._shadowRoot.appendChild(templateForm.content.cloneNode(true));
        
        // this.$userName = $user;
        // this.$titleName = $title;
        // this.$user = this._shadowRoot.querySelector('.userTag');
        this.$title = this._shadowRoot.querySelector('.title');
        this.$img = this._shadowRoot.querySelector('.imgTag');
        // this.$fileTag = this._shadowRoot.querySelector('#file');
        // this.$btn = this._shadowRoot.querySelector('#download');
        // this.$btn.addEventListener('change', this._fileSelect.bind(this));
        // this.$btnEnvoi = this._shadowRoot.querySelector('#btnEnvoi');
        // this.$btnEnvoi.addEventListener('click', this._addMsg.bind(this));
        this._fileSelect;
        // this._userVal;
        this._todos = [];
    }
    
    _addMsg(){
        if(this.$title.value.length > 0){
            this._todos.push({ title: this.$title.value, checked: false })
            // console.log(this._todos);
            this.$title.value = '';
        }
        if(this.$img.value.length > 0){
            this.$img.innerHTML = 'https://cdn.discordapp.com/embed/avatars/0.png';
            this._todos.push({ imgUrl: this.$img.value, checked: false });
            this.$img.value = '';
        }
        // console.log(this._todos);
        this.getData();
        // console.log(this.getData());
    }

    // _fileSelect(evt) {
    //     let file = evt.target.files;
    //     console.log(file);
    //     $fileTag.innerHTML = 'Ici!';
    //     var output = [];
    // }

    getData(){
        return {
            title: this.$title.value,
            img: this.$img.value
        }
    }
    getEndForm(){
        this.$title.value = '';
        this.$img.value = '';
        return;
    }

}
window.customElements.define('app-formtag', FormTag);

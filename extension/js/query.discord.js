// const data = document.querySelector('.title').getData();
// username: data.title,

// import FormTag from './form.component';
// import TagName from './tag.component';
// new FormTag();
// new TagName();

export default class Discord extends HTMLElement {
  constructor() {
    super();
    this._shadowRoot = this.attachShadow({mode: 'open'});
    this._shadowRoot.innerHTML = `
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <div id="container">
        <app-formtag></app-formtag>
        <app-tagname></app-tagname>
        <br />
        <button type="button" class="btn btn-success" id="btnEnvoi">Envoyer</button>
        <br />
      </div>
    `;
    // this.$btnEnvoi = this._shadowRoot.addEventListener('click', this._query.bind(this));
    this.$btn = this._shadowRoot.querySelector('#btnEnvoi');
    this.$btn.addEventListener('click', this._query.bind(this));
  }
  _query(e){
    this.$appForm = this._shadowRoot.querySelector('app-formtag');
    // console.log(this.$appForm.getData());
    this.$appTag = this._shadowRoot.querySelector('app-tagname');
    // console.log(this.$appTag.getTag());
    // e.stopPropagation();
    this._getDone();
  }
  _getDone(){
    let data = this.$appForm.getData();
    // console.log(data);
    let todos = this.$appTag.getTag();
    // console.log(todos);
    let values = Object.values(todos);
    const vals = {
        author : "Yannick",
        pseudo : "Cursus",
        title : data.title,
        img : data.img,
        tag : todos,
        url : "http://lorempixel.com/250/150",
        content : "it\'s a good days"
    }

    const message = 
    {
      "username": "Cursus",
      "content": "it s a good days 😃 ```js\nfunction foo(bar) {\n  console.log(bar);\n}\n\nfoo(1);```",
      "embeds": [{
        "title": "Les composants universels",
        "description": "Sans quoi les bases de ce monde s'éléveraient",
        "url": "https://discordapp.com",
        "color": 8513155,
        "timestamp": "2019-05-27T07:04:30.851Z",
        "image": {
          "url": "http://lorempixel.com/250/150"
        },
        "author": {
          "name": "Cursus"
        },
        "fields": [
          {
            "name": "Titre",
            "value": vals.title,
          },
          {
            "name": "Description",
            "value": "Expliquer en deux mots?"
          },
          {
            "name": "Tags",
            "value": "css, javascript et python"
          },
          {
            "name": "Url",
            "value": vals.url
          }
        ]
      }]
  }
    this._sendMessage(message);
  }
  
  _sendMessage(message){
    // console.log(message);
    // .... code d'envoi
    const DISCORD_WEBHOOK_URL = "https://discordapp.com/api/webhooks/580038638771896339/hZso8WiRVFyfP0dRB1FhZnrVqhnwkRzUy1WpKJ5c_ibj2yb0QsB9WN6K9SLcDN6ludx0";
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    fetch(DISCORD_WEBHOOK_URL, { 
          method: "POST",
          body: JSON.stringify(message),
          headers
      })
    .then(res => res.json())
    .then(d => console.log(d))
    .catch(e => console.error(e))

    // console.log(message);
  }
} window.customElements.define('app-discord', Discord);
const template = document.createElement('template');
template.innerHTML = `
<style type="text/css">
    #container {
        width: 500px
        position: relative;
    }
    .input-group.flex-nowrap {
        width: 250px;
    }
    .badge {
        marginleft: 2px;
    }
    .badge.badge-secondary .spanItem {
        margin-left: 0;
        padding: 0;
    }
</style>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<div id="container">
    <div class="input-group flex-nowrap">
    <div class="input-group-prepend">
        <span class="input-group-text" id="addon-wrapping">Tag</span>
    </div>
    <input type="text" class="form-control inputTag" placeholder="Entrer votre tag!" aria-label="Username" aria-describedby="addon-wrapping" >
    <div class="input-group-append">
        <button type="button" class="btn btn-success"id="btnTag">&radic;</button>
    </div>
    </div>
    
    <!-- <button type="button" class="btn btn-danger" id="btnClear">x</button><br /> -->
    <span id="spanTag" ></span>
    <div id="divTag"></div>
    
<div>
`;

export default class TagName extends HTMLElement {
    constructor() {
        super();
        this._shadowRoot = this.attachShadow({mode: 'open'});
        this._shadowRoot.appendChild(template.content.cloneNode(true));
        // this._shadowRoot.innerHTML = '<strong>Hello</strong>';
        this.$input = this._shadowRoot.querySelector('.inputTag');
        this.$btn = this._shadowRoot.querySelector('#btnTag');
        this.$clear = this._shadowRoot.querySelector('#btnClear');
        this.$span = this._shadowRoot.querySelector('#spanTag');
        this.$div = this._shadowRoot.querySelector('#divTag');
        
        this.$btn.addEventListener('click', this._addTodo.bind(this));
        // this.$clear.addEventListener('click', this._remove.bind(this));
        this._todos = [];
    }
    _addTodo() {
        if(this.$input.value.length > 0){
            this._todos.push({ tag: this.$input.value, checked: false })
            // console.log(this._todos);
            this._renderTodoList();
            this.$input.value = '';
        }
    }
    _renderTodoList() {
        this.$div.innerHTML = '';
        this._i = 0;
        this._todos.forEach((todo, index) => {
            let $item = document.createElement('h5');
            $item.className = 'badge badge-info';
            $item.innerHTML = todo.tag;
            this.$div.appendChild($item);
            
            let $spanItem = document.createElement('span');
            $spanItem.className = 'badge badge-danger';
            $spanItem.setAttribute('id', this._i++);
            $spanItem.innerHTML = 'x';
            this.$div.appendChild($spanItem);

            $spanItem.addEventListener('click', this._remove.bind(this));
        });
        this.getTag();
        // console.log(this.getTag());
    }
    _remove() {
        this._clear();
        this._clear1();
        this.getEndTag();
    }
    _clear() {
        if(this._todos.length > 0) {
            this._todos.pop();
            let clear = this.$div.querySelector('.badge:last-child');
            this.$div.removeChild(clear);
            // console.log(this._todos)
        }
    }
    _clear1() {
            let clear1 = this.$div.querySelector('.badge:last-child');
            this.$div.removeChild(clear1);
    }

    getTag(){
        return {
            tag: this._todos
        }
    }
    getEndTag(){
        let $divSpan = this.shadowRoot.getElementById('#divTag').innerHTML = '';
        console.log($divSpan);
        // while($divSpan.firsChild){
        //     $divSpan.removeChild($divSpan.firsChild);
        // }
        return;
    }
}
window.customElements.define('app-tagname', TagName);